from main import IM_REGEX


def test_regex_apostrophe_1():
    assert IM_REGEX.match("I'm a test.")


def test_regex_apostrophe_2():
    assert IM_REGEX.match("I’m a test.")


def test_regex_no_apostrophe():
    assert IM_REGEX.match("Im a test.")


def test_regex_preceding_comma():
    assert IM_REGEX.match(", I'm a test.")


def test_regex_preceding_letters():
    assert not IM_REGEX.match("bim a test.")
