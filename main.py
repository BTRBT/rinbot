import re  # Kill me.
import os
import string
import logging
from random import choice

import discord
from dotenv import load_dotenv


load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
PATTERN = r"^(?:.*\s)?(?:i’m|i'm|i am|im)\s+(.+?|\<\@\!.+?)(?:\?|\!|\.|\,|$)"
IM_REGEX = re.compile(PATTERN, re.IGNORECASE)
GREETINGS = ["Hello", "Hey there", "Hi", "Meowdy", "Hey", "Hiya"]

intents = discord.Intents(messages=True)


class Client(discord.Client):
    async def on_ready(self):
        logging.info(f"{self.user} has connected to Discord.")

    async def on_message(self, message):
        if not message.author == self.user:
            match = IM_REGEX.match(message.content)

            if match:
                logging.info("Someone sent a matching message.")

                # This is not a cryptographic function. Random is fine.
                greeting = choice(GREETINGS)  # nosec B311
                name = string.capwords(match.group(1))
                kek_emoji = "<:max_kek:886828947529150494>"

                await message.reply(
                    f"{greeting}, {name}. I'm Rin! {kek_emoji}")


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    client = Client()
    client.run(TOKEN)
